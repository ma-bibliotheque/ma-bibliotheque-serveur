import hashlib
from string import Template
import requests
from lxml import etree

from api import xml


class BNF:
    DATA_API_URL = "https://catalogue.bnf.fr/api/SRU"

    DATA_VERSION = "1.2"
    DATA_OPERATION = "searchRetrieve"
    DATA_RESPONSE_FORMAT = "unimarcXchange"

    DATA_NS = {"srw": "http://www.loc.gov/zing/srw/",
               "m": "http://catalogue.bnf.fr/namespaces/InterXMarc",
               "mn": "http://catalogue.bnf.fr/namespaces/motsnotices",
               "mxc": "info:lc/xmlns/marcxchange-v2",
               "dc": "http://purl.org/dc/elements/1.1/",
               "oai_dc": "http://www.openarchives.org/OAI/2.0/oai_dc/",
               }

    DATA_PARAMS = {"version": DATA_VERSION,
                   "operation": DATA_OPERATION,
                   "recordSchema": DATA_RESPONSE_FORMAT,
                   }

    DATA_XPATH_TEMPLATE = Template("//srw:record/srw:recordData/mxc:record/"
                                   "mxc:datafield[@tag='$tag']/"
                                   "mxc:subfield[@code='$code']")

    DATA_XPATH_DICT = {
        "title": (200, "a", None),
        "author_last_name": (700, "a", None),
        "author_first_name": (700, "b", None),
        "editor": (214, "c", None),
        "editor_old": (210, "c", None),
        "edition_date": (214, "d", xml.XML.parse_edition_date),
        "edition_date_old": (210, "d", xml.XML.parse_edition_date),
        "summary": (330, "a", None),
    }

    COVER_API_URL = "http://catalogue.bnf.fr/couverture"

    COVER_PARAMS = {
        "appName": "NE",
        "idArk": "",
        "couverture": 1,
    }

    NO_COVER_HASH = "417f42c24c95425855d9f918909f7be8de44bb2025362ea4d8afca706dec28a5" \
                    "0e0df2978688fee6bb3108a3727cd837cf2776b6784816691ef65649ec6217eb"

    @classmethod
    def book_data(cls, isbn):
        params = cls.DATA_PARAMS.copy()
        params["query"] = f'bib.isbn adj "{isbn}"'
        api_call = requests.get(cls.DATA_API_URL, params=params)
        xml_root = etree.fromstring(api_call.content)

        result = dict()
        result["api_url"] = api_call.url

        # id_ark is managed separately because it has a different XPath
        id_ark = xml_root.xpath("//mxc:record", namespaces=cls.DATA_NS, )
        result["id_ark"] = id_ark[0].attrib["id"] if len(id_ark) else ""

        for k, v in cls.DATA_XPATH_DICT.items():
            data = xml_root.xpath(
                cls.DATA_XPATH_TEMPLATE.substitute({"tag": v[0],
                                                    "code": v[1]}),
                namespaces=cls.DATA_NS,
            )

            data = data[0].text if len(data) else None

            result[k] = (v[2](data)
                         if data is not None and v[2] is not None
                         else data
                         )

        # Clean editor data
        if not any((result["editor"], result["edition_date"],)):
            result["editor"], result["edition_date"] = (
                result["editor_old"], result["edition_date_old"])
            result.pop("editor_old", None)
            result.pop("edition_date_old", None)

        return result

    @classmethod
    def book_cover(cls, id_ark):
        params = cls.COVER_PARAMS.copy()
        params["idArk"] = id_ark
        cover_data = requests.get(url=cls.COVER_API_URL, params=params)

        cover_exists = (True if hashlib.blake2b(cover_data.content).hexdigest() != cls.NO_COVER_HASH
                        else False)

        return cover_data.url if cover_exists else None
