from datetime import datetime


class XML:

    @staticmethod
    def parse_edition_date(text):
        return datetime(year=int(text[-4:]), month=1, day=1)
