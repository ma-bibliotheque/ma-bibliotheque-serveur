import requests


class Qwant:

    API_URL = "https://api.qwant.com/v3/search/images"

    HEADERS = {"User-Agent": "Mozilla/5.0 (Linux x86_64; rv:88.0) Gecko/20100101 Firefox/88.0",
               }

    PARAMS = {
              "locale": "fr_fr",
              "count": 10,
              }

    @classmethod
    def search(cls, isbn, title="", author_name=""):

        def parse(json):
            return [{'url': i['media_fullsize'],
                     'image_type': i['thumb_type'],
                     }
                    for i in json['data']['result']['items']]

        params = cls.PARAMS.copy()

        cover_url_list = []

        search_list = [
            {'q': isbn, 'size': "large"},
            {'q': isbn, 'size': "medium"},
        ]

        if any((title, author_name)):
            search_list.extend([
                {'q': " ".join([title, author_name]), 'size': "large"},
                {'q': " ".join([title, author_name]), 'size': "medium"},
            ])

        for search in search_list:
            params['q'] = search['q']
            params['size'] = search['size']
            params['count'] = cls.PARAMS['count'] - len(cover_url_list)
            r = requests.get(cls.API_URL, params=params, headers=cls.HEADERS)

            cover_url_list.extend(
                parse(
                    r.json()
                )
            )

            if len(cover_url_list) == cls.PARAMS['count']:
                return cover_url_list

        return cover_url_list
