from django.http import JsonResponse
from django.views.decorators.http import require_POST
from django.contrib.auth import authenticate, login as _login, logout as _logout
from django.views.decorators.csrf import ensure_csrf_cookie

from .models import Book


def build_json_response(name, user, data=None):

    user_response = None

    if user.is_authenticated:
        user_response = {
            'id': user.id,
            'username': user.username
        }

    response = {
        'name': name,
        'user': user_response,
    }

    if data is not None:
        response['data'] = data

    return JsonResponse(response)


@ensure_csrf_cookie
def connexion_status(request):

    user = request.user
    data = {'connected': False}

    if request.user.is_authenticated:
        data['connected'] = True

    return build_json_response('connexion_status', user, data)


@require_POST
def login(request):
    username = request.POST.get('username', '')
    password = request.POST.get('password', '')
    user = authenticate(username=username, password=password)

    if user is not None:
        _login(request, user)

    return build_json_response('login', user)


def logout(request):
    _logout(request)
    return build_json_response('logout', request.user)


def book_list(request):

    user = request.user
    data = None

    if request.user.is_authenticated:
        data = {'books': list(Book.objects.filter(user=user).all().values())}

    return build_json_response('book_list', user, data)
