import datetime
import requests
from pathlib import Path

from django.db import models
from django.core.files.base import ContentFile
from django.conf import settings
from django.core.validators import MinValueValidator, MaxValueValidator
from django.contrib.auth.models import AbstractUser

from api import COVERS_DIR
from api import bnf, qwant


def covers_dir_path(instance, filename):
    return Path("covers", instance.book.isbn, filename)


class User(AbstractUser):
    pass


class Author(models.Model):
    last_name = models.CharField(max_length=30, default=None,
                                 blank=True, null=True)
    first_name = models.CharField(max_length=30, default=None,
                                  blank=True, null=True)


class Editor(models.Model):
    name = models.CharField(max_length=100, default=None,
                            blank=True, null=True)


class Book(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='books')

    isbn = models.CharField(max_length=13, unique=True)
    id_ark = models.CharField(max_length=50)
    title = models.CharField(max_length=100, blank=True, null=True,
                             default=None)
    author = models.ForeignKey(Author, on_delete=models.CASCADE, blank=True,
                               null=True, default=None)
    editor = models.ForeignKey(Editor, on_delete=models.CASCADE, blank=True,
                               null=True, default=None)
    edition_date = models.DateField(blank=True, null=True, default=None)
    summary = models.TextField(blank=True, null=True, default=None)
    score = models.PositiveSmallIntegerField(
        validators=[MinValueValidator(0), MaxValueValidator(5)],
        blank=True,
        null=True,
        default=None,
    )
    reading_date = models.DateField(default=datetime.date.today)
    creation_date = models.DateField(auto_now_add=True)
    validated = models.BooleanField(default=False)

    @staticmethod
    def create_with_isbn_data(isbn):
        data = bnf.BNF.book_data(isbn=isbn)

        author = Author.objects.create(
            last_name=data["author_last_name"],
            first_name=data["author_first_name"],
        )

        editor = Editor.objects.create(
            name=data["editor"],
        )

        book = Book.objects.create(
            isbn=isbn,
            id_ark=data["id_ark"],
            title=data["title"],
            author=author,
            editor=editor,
            edition_date=data["edition_date"],
            summary=data["summary"],
        )

        return book

    def fetch_covers_url(self):
        bnf_cover_url = bnf.BNF.book_cover(id_ark=self.id_ark)
        qwant_covers_urls = qwant.Qwant.search(isbn=self.isbn,
                                               title=self.title,
                                               author_name=self.author.last_name,
                                               )

        if bnf_cover_url is not None:
            Cover.objects.create(
                book=self,
                image=None,
                source="Bibliothèque Nationale de France",
                source_url=bnf_cover_url,
                order=1,
            )

        for i, qwant_cover_url in enumerate(qwant_covers_urls):
            Cover.objects.create(
                book=self,
                image=None,
                image_type=qwant_cover_url['image_type'],
                source="Qwant",
                source_url=qwant_cover_url['url'],
                order=i + 1,
            )

    def download_next_cover(self):
        cover = self.covers.filter(image='').order_by('pk').first()

        if cover is not None:
            try:
                cover.download()
                return cover
            except BadUrlError:
                cover.image.delete()
                cover.delete()
                self.download_next_cover()

        else:
            return None


class Cover(models.Model):
    book = models.ForeignKey(Book, on_delete=models.CASCADE, related_name='covers')
    image = models.ImageField(upload_to=covers_dir_path,
                              blank=True, null=True, default=None)
    image_type = models.CharField(max_length=4, null=True, default=None)
    source = models.CharField(max_length=50)
    source_url = models.URLField(blank=True, null=True, default=None)
    order = models.IntegerField()

    def download(self, force=False):
        result = requests.get(self.source_url)

        if result.status_code == 200:
            content = ContentFile(result.content)
            dir_path = settings.MEDIA_ROOT / 'covers' / f"{self.book.isbn}"
            dir_path.mkdir(parents=True, exist_ok=True)
            filename = (self.source.lower()
                        + (f"_{self.order}" if self.order is not None else "")
                        + f".{self.image_type}")

            file_path = dir_path / filename

            if file_path.exists() and force is not True:
                raise FileExistsError

            else:
                self.image.save(name=filename, content=content)

        else:
            raise BadUrlError


class BadUrlError(Exception):
    pass
