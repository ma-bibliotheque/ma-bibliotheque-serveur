from pathlib import Path

from django.conf import settings


COVERS_DIR = Path(settings.MEDIA_ROOT, 'covers/')
