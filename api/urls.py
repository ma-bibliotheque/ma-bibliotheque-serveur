from django.urls import path

from . import views

urlpatterns = [
    path('connexion_status', views.connexion_status, name='connexion_status'),
    path('login', views.login, name='login'),
    path('logout', views.logout, name='logout'),
    path('book_list', views.book_list, name='book_list'),
]
